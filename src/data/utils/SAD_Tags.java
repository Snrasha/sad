/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.data.utils;

/**
 *
 * @author Willefert
 */
public class SAD_Tags {
    	public static final String SAD_FACTION = "sad";
	public static final String THEME_SAD = "theme_sad";
	public static final String THEME_SAD_MAIN = "theme_sad_main";
        public static final String SAD_STATION="sad_station";
        public static final String SAD_WARNING_BEACON="sad_warning_beacon";
        public static final String SAD_WARNING_BEACON2="sad_warning_beacon2";
	public static final String THEME_SAD_SECONDARY = "theme_sad_secondary";
	public static final String THEME_SAD_DESTROYED = "theme_sad_destroyed";
	public static final String THEME_SAD_SUPPRESSED = "theme_sad_suppressed";
	public static final String THEME_SAD_RESURGENT = "theme_sad_resurgent";
	public static final String THEME_BREAKER = "theme_breakers";
}
